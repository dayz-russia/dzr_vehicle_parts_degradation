enum DZR_VPD_IO_Command
{
	GET			= 0,
	DELETE			= 1,
	APPEND			= 2,
	GET_ARRAY			= 3,
}
enum DZR_VPD_IO_Flag
{
	GET			= 0,
}

modded class CarScript
{	
	
	float m_KmBeforeRuined = 2;
	float m_TickPassed;
	float m_FiveSpassed;
	float m_OdometerTickMeters = 10;
	float m_OdometerMeters = 0.0;
	float m_Tick = 1;
	float m_DamageVariation = -0.5;
	
	string m_DebugEnable = "0";
	string m_AttractAI = "1";
	
	
	float m_theRandom;
	int m_TotalOdometer;
	
	ref array<ItemBase> m_Blown = new array<ItemBase>;
	ref array<string> m_IgnoredVehiclesWheels = new array<string>;
	
	ref array<string> m_NullArray = new array<string>;
	string m_NullString = "";
	
	static bool isDiagServer;
	static bool isDiagMPClient;
	bool m_Ignore = false;
	int m_NextVoNNoiseTime;
	vector carPos;
	
	void CarScript()
	{
		GetRPCManager().AddRPC( "DZR_VPD", "TireExplodeClientside", this, SingleplayerExecutionType.Client );	
		
		//taken from VPPAdminTools
		isDiagServer 		 = GetGame().IsMultiplayer() && GetGame().IsDedicatedServer();   //Diag server
		isDiagMPClient		 = GetGame().IsMultiplayer() && !GetGame().IsDedicatedServer();  //Diag client 
		//taken from VPPAdminTools
		
		m_KmBeforeRuined = DZR_VPD_GetFile("$profile:\\DZR\\Vehicle Parts Degradation", "KmBeforeTiresExplode.txt", m_NullArray).ToFloat();
		m_DamageVariation = DZR_VPD_GetFile("$profile:\\DZR\\Vehicle Parts Degradation", "TireRandomDamage.txt", m_NullArray).ToFloat();
		m_DebugEnable = DZR_VPD_GetFile("$profile:\\DZR\\Vehicle Parts Degradation", "DebugEnable.txt", m_NullArray);
		m_AttractAI = DZR_VPD_GetFile("$profile:\\DZR\\Vehicle Parts Degradation", "TiresExplosionAttractsAI.txt", m_NullArray);
		string m_nuller = DZR_VPD_GetFile("$profile:\\DZR\\Vehicle Parts Degradation", "IgnoredVehiclesWheels.txt", m_IgnoredVehiclesWheels, m_NullString, DZR_VPD_IO_Command.GET_ARRAY);
		// Print("================m_IgnoredVehiclesWheels========");
		// Print(m_IgnoredVehiclesWheels);
		// Print(this.GetType())
		// Print(m_IgnoredVehiclesWheels.Find( this.GetType() ))
		if( m_IgnoredVehiclesWheels.Find( this.GetType() ) != -1 )
		{
			m_Ignore = true;
		}
	}
	
	void TireExplodeClientside(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		// Param2<string, string> params;
		// if (!ctx.Read(params))
		// return;
		//Print("===================== RPC EXPLODE ON CLIENT ==================");
        if ( GetGame().IsClient() || !GetGame().IsMultiplayer() )
        {
            EffectSound sound;
            PlaySoundSet( sound, "DZR_VPD_ExplosionSoundSet", 0.0, 0.0 );
		}
	}
	
	override void OnUpdate( float dt )
    {
		
		
		
		if ( GetGame().IsServer() & !m_Ignore)
		{
			float m_SpeedKMh = GetVelocity(this).Length() * 3.6;
			
			//float mBodyMass = dBodyGetMass(this);
			
			float m_SpeedMPS = m_SpeedKMh / 60 / 60;
			int pos;
			
			
			//if is moving back or forward
			if ( GetVelocity(this).Length() > 0.1 || GetVelocity(this).Length() < -0.1 )
			{
				
				// Check surface
				/*
					string surfaceType = GetSurfaceType();
					switch (surfaceType)
					{
					case "asphalt_ext":
					{
					
					}
					}
				*/
				
				int now = GetGame().GetTime();
				if ( now >= m_NextVoNNoiseTime )
				{
					m_NextVoNNoiseTime = now + 1000;
					vector curPos = this.GetWorldPosition();
					if(carPos == "0 0 0")
					{
						carPos = curPos;
					}
					float distance = vector.Distance(carPos, curPos);
					carPos = curPos;
					m_TotalOdometer += Math.Round(distance) ;
					
				}
				
				m_TickPassed += dt;
				
				//meters per second
				m_OdometerMeters += GetVelocity(this).Length();
				//m_TotalOdometer += GetVelocity(this).Length();
				//wait until the tick collected
				if  ( (m_OdometerMeters / 100) > m_OdometerTickMeters) 
				{
					if(m_DebugEnable == "1")
					{
						
						GlobalMessage("ODOMETER: "+m_TotalOdometer+" m | Nominal Ruined Distance: "+m_KmBeforeRuined+" Km");
					}
					// *dt to get damage per second
					for ( int j = 0; j < GetInventory().AttachmentCount(); j++ )
					{
						
						
						
						ItemBase attachment = ItemBase.Cast(GetInventory().GetAttachmentFromIndex(j));
						
						//! ignore all spare wheel
						int slotId;
						string slotName;		
						attachment.GetInventory().GetCurrentAttachmentSlotInfo(slotId, slotName);
						slotName.ToLower();
						if (slotName.Contains("spare"))
						{
							attachment = NULL;
						}
						
						if ( attachment && (attachment.GetHealth() > 1) )
						{
							(m_Blown.Find( attachment ) != -1 )
							{
								pos = m_Blown.Find( attachment );
								m_Blown.Remove(pos);
							}
							//attachment.SetHealthMax("","");
							if ( attachment.IsInherited(CarWheel) )
							{
								
								//m_DamagePerTick = attachment.GetMaxHealth() / ( ( m_KmBeforeRuined * 100) * m_OdometerTickMeters);
								float m_DamagePerTick = 0;
								//m_DamagePerTick = ( (m_KmBeforeRuined*1000 / attachment.GetMaxHealth()) ) ;
								m_DamagePerTick = attachment.GetMaxHealth() / (m_KmBeforeRuined*1000 / m_OdometerTickMeters);
								m_theRandom = Math.RandomFloatInclusive(0, m_DamageVariation);
								attachment.AddHealth(-m_DamagePerTick);
								attachment.AddHealth(-m_theRandom*m_DamagePerTick);
								//m_TickPassed += Math.RandomFloatInclusive(0, m_TickVariation);
								if(m_DebugEnable == "1")
								{
									
									GlobalMessage("[Wheel "+attachment+" | Health:"+attachment.GetHealth()+"] Damage: "+(m_DamagePerTick - m_theRandom*m_DamagePerTick));
									/*
										Print("------------------------");
										// Print("GetVelocity(this).Length()");
										// Print(GetVelocity(this).Length());
										//Print(m_Blown);
										Print(attachment);
										//Print("attachment.AddHealth(-m_DamagePerTick - (Math.RandomFloatInclusive(-m_DamageVariation, m_DamagePerTick)) );");
										
										Print("m_DamagePerTick: "+m_DamagePerTick);
										Print("m_theRandom*m_DamagePerTick: "+m_theRandom*m_DamagePerTick);
										//Print(attachment.GetMaxHealth() / ( (m_KmBeforeRuined * 100) * m_OdometerTickMeters ));
										Print("attachment.GetMaxHealth()");
										Print(attachment.GetMaxHealth());
										// Print("m_KmBeforeRuined * 1000");
										// Print(m_KmBeforeRuined * 1000);
										// Print("m_OdometerTickMeters");
										// Print(m_OdometerTickMeters);
										Print("attachment.GetHealth()");
										Print(attachment.GetHealth());
										Print("m_OdometerMeters: "+m_OdometerMeters);
										Print("m_TotalOdometer");
										Print(m_TotalOdometer / 100);
									*/
								}
								
								m_DamagePerTick = 0;
								
								
							}
							
							/*
								if ( attachment.IsInherited(CarDoor) )
								{
								array<string> att_dmgZones = new array<string>;
								attachment.GetDamageZones(att_dmgZones);
								foreach (string att_dmgZone: att_dmgZones)
								{
								attachment.SetHealthMax( att_dmgZone, "Health" );
								}
								}
							*/
						} 						
						else
						{
							if(m_Blown.Find( attachment ) == -1 && attachment)
							{
								GetRPCManager().SendRPC( "DZR_VPD", "TireExplodeClientside", NULL, true);
								m_Blown.Insert(attachment);
								attachment.AddHealth(-100);
								if(m_DebugEnable == "1")
								{
									Print("===================== WHEEL "+attachment+" EXPLODED ON SERVER ==================");
								}
								
								if(m_AttractAI == "1")
								{
									m_NoisePar = new NoiseParams();
									if ( m_NoisePar )
									{
										m_NoisePar.LoadFromPath( "CfgVehicles DZR_VPD_Explosion NoiseTireExplosion" );
										NoiseSystem noise = GetGame().GetNoiseSystem();
										if ( noise )
										{
											noise.AddNoiseTarget( this.GetPosition(), 5.0, m_NoisePar );
											
											
										}
									}
								}
							}
						}
						
					}
					m_OdometerMeters = 0;
				}
			}
			else
			{
				m_TickPassed = 0;
			};
			
			
			// For visualisation of brake lights for all players
			/*
				float brake_coef = GetBrake();
				if ( brake_coef > 0 )
				{
				if ( !m_BrakesArePressed )
				{
				m_BrakesArePressed = true;
				SetSynchDirty();
				OnBrakesPressed();
				}
				}
				else
				{
				if ( m_BrakesArePressed )
				{
				m_BrakesArePressed = false;
				SetSynchDirty();
				OnBrakesReleased();
				}
				}
				
				if ( (!GetGame().IsDedicatedServer()) && m_ForceUpdateLights )
				{
				UpdateLights();
				m_ForceUpdateLights = false;
				}
			*/
		}
		
		super.OnUpdate( dt );
	}	
	
	static string DZR_VPD_GetFile(string folderPath, string fileName, out ref array<string> contentsArray, string newContents = "", int IO_Command_DZRVPD = DZR_VPD_IO_Command.GET, string execSide = "Both", bool NoChangeIfExists = 0)
	{
		string m_TxtFileName = fileName;
		string fileContent;
		FileHandle fhandle;
		string defaultContents;
		string pth = folderPath +"/"+ m_TxtFileName;
		
		if ( FileExist(folderPath +"/"+ m_TxtFileName) && (IO_Command_DZRVPD == DZR_VPD_IO_Command.GET || IO_Command_DZRVPD == DZR_VPD_IO_Command.APPEND || DZR_VPD_IO_Command.GET_ARRAY) )
		{
			
			
			//file
			fhandle	=	OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.READ);
			//string server_id;
			if(IO_Command_DZRVPD != DZR_VPD_IO_Command.GET_ARRAY)
			{
				FGets( fhandle,  fileContent );
			}
			else
			{
				bool stopArray = false;
				string line_content;
				while ( FGets( fhandle,  line_content ) > 0 && !stopArray)
				{
					if(line_content == "" || line_content == "\r\n" || line_content == " ") 
					{
						stopArray = true;
					}
					else
					{
						contentsArray.Insert(line_content);
					}
				}
				fileContent = contentsArray[0];
			}
			
			CloseFile(fhandle);
			
			if(!NoChangeIfExists)
			{
				if(newContents != "")
				{
					if(newContents != fileContent)
					{
						fileContent = newContents;
						
						if( IO_Command_DZRVPD == DZR_VPD_IO_Command.APPEND)
						{
							fhandle = OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.APPEND);
						}
						else 
						{
							fhandle = OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.WRITE);
						};
						
						FPrintln(fhandle, fileContent);
						CloseFile(fhandle);
					};
					
					Print("[dzr_framework] "+execSide+" :::  File "+folderPath +"/"+ m_TxtFileName+" Updated to: "+fileContent);
				}
			}
			
			//Print("[dzr_framework] "+execSide+" :::  File "+folderPath +"/"+ m_TxtFileName+" is OK! Contents: "+fileContent);
			//AddToGlobalConfig(m_TxtFileName, fileContent);
			return fileContent;
		}
		else 
		{
			// checking dirs
			TStringArray parts();
			string path = folderPath;
			path.Split("/", parts);
			path = "";
			foreach (string part: parts)
			{
				path += part + "/";
				if (part.IndexOf(":") == part.Length() - 1)
				continue;
				
				if(IO_Command_DZRVPD == DZR_VPD_IO_Command.GET)
				{
					if (!FileExist(path) && !MakeDirectory(path))
					{
						Print("Could not make dirs from path: " + path);
						return "Could not make dirs from path: " + path;
					}
				}
				
				if(IO_Command_DZRVPD == DZR_VPD_IO_Command.DELETE)
				{
					DeleteFile(pth);
					Print("Deleted file: "+pth);
					return "Deleted file: "+pth;
				}
			}
			
			//Default new file
			FileHandle file = OpenFile(pth, FileMode.WRITE);
			if(newContents == "")
			{
				newContents = "You tried to create a file using dzr_framework, but did not provide file contents. So this default content is added.";
			}
			if (file != 0 && IO_Command_DZRVPD == DZR_VPD_IO_Command.GET)
			{
				FPrintln(file, newContents);
				CloseFile(file);
				//file
				fhandle	=	OpenFile(folderPath +"/"+ m_TxtFileName, FileMode.READ);
				//string server_id;
				FGets( file,  fileContent );
				
				Print("[dzr_framework] "+execSide+" :::  File "+folderPath +"/"+ m_TxtFileName+" is OK! Contents: "+fileContent);
				CloseFile(fhandle);
				//AddToGlobalConfig(m_TxtFileName, fileContent);
				return fileContent;
			}
		}
		//Print("[dzr_framework] isDiagServer:"+isDiagServer+" isDiagMPClient:"+isDiagMPClient+" :::  File "+folderPath +"/"+ m_TxtFileName+" ERROR!");
		return "[dzr_framework] isDiagServer:"+isDiagServer+" isDiagMPClient:"+isDiagMPClient+" :::  File "+folderPath +"/"+ m_TxtFileName+" ERROR!";
	}
	
	void GlobalMessage(string msg)
	{
		Param1<string> Msg;	
		array<Man> m_Players = new array<Man>;
		int pCounter;
		GetGame().GetWorld().GetPlayerList( m_Players );
		if ( m_Players.Count() > 0 )
		{
			//Print("[SMM] Sending mission radio frequency info to all clients.");
			for ( int k = 0; k < m_Players.Count(); k++ )
			{
				PlayerBase player = PlayerBase.Cast( m_Players.Get( pCounter ) );
				player.OnTick();
				Msg = new Param1<string> (msg);
				GetGame().RPCSingleParam( player, ERPCs.RPC_USER_ACTION_MESSAGE, Msg, true, player.GetIdentity());
				pCounter++;
			}
		}
		
		
		
	}
}