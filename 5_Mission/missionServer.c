modded class MissionServer
{
	
	ref array<string> m_NullArray = new array<string>;
	string m_NullString = "";
	
	void MissionServer()
	{
		Print("[dzr_vehicle_parts_degradation] ::: Starting Serverside");
		
		CarScript.DZR_VPD_GetFile("$profile:/DZR/Vehicle Parts Degradation", "KmBeforeTiresExplode.txt", m_NullArray, "500\r\n\r\n(Km) Nominal distance before the wheels explode. Reduced by random damage TireRandomDamage.\r\nНоминальная дистанция (км) до повреждения покрышек. Сокращается рандомным уроном TireRandomDamage", DZR_VPD_IO_Command.GET, "Server", true );
		CarScript.DZR_VPD_GetFile("$profile:/DZR/Vehicle Parts Degradation", "TireRandomDamage.txt", m_NullArray, "4\r\n\r\nEach wheel additional damage between 0 and this number.\r\nДополнительный рандомный износ от 0 до этого значения.", DZR_VPD_IO_Command.GET, "Server", true );
		CarScript.DZR_VPD_GetFile("$profile:/DZR/Vehicle Parts Degradation", "DebugEnable.txt", m_NullArray, "0\r\n\r\n1 = Show odometer + each wheel damage per\r\n1 = Показывает одометр и износ каждого колеса.", DZR_VPD_IO_Command.GET, "Server", true );
		CarScript.DZR_VPD_GetFile("$profile:/DZR/Vehicle Parts Degradation", "TiresExplosionAttractsAI.txt", m_NullArray, "1\r\n\r\n1 = Wheel explosion sound will attract the infected.\r\n1 = Хлопок от взрыва колеса привлекает инфицированных.", DZR_VPD_IO_Command.GET, "Server", true );
		CarScript.DZR_VPD_GetFile("$profile:/DZR/Vehicle Parts Degradation", "IgnoredVehiclesWheels.txt", m_NullArray, "ThisVehicleClassWillNotGetWheelDamage\r\nThisVehicleClassWillNotGetWheelDamage_Too\r\n\r\nVehicles ignored by the wheel degradation. Each vehicle at new line. List ends on empty line.\r\nСписок транспорта, который игнорится износом колёс.", DZR_VPD_IO_Command.GET, "Server", true );
	}
}